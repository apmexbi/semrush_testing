import requests
import config

# Read Docs for API Config description
# https://www.semrush.com/api-analytics/#backlinks
TYPE = 'backlinks'
KEY = config.SEMRUSH_API
TARGET = 'apmex.com'
TARGET_TYPE = 'root_domain'
EXPORT_COLUMNS = ('page_ascore,source_title,source_url,target_url,'
                  'anchor,external_num,internal_num,first_seen,last_seen')
DISPLAY_SORT = None
DISPLAY_LIMIT = '3'
DISPLAY_OFFSET = None
DISPLAY_FILTER = None

api_url = (
        f'https://api.semrush.com/analytics/v1/'
        f'?type={TYPE}'
        f'&key={KEY}'
        f'&target={TARGET}'
        f'&target_type={TARGET_TYPE}'
        f'&export_columns={EXPORT_COLUMNS}'
        f'&display_limit={DISPLAY_LIMIT}'
    )

response = requests.get(api_url)

print(response.text)
