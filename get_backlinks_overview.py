# This report provides a summary of backlinks, including their type, referring domains
# and IP addresses for a domain, root domain, or URL.

import config
from datetime import datetime
import pandas as pd
import pandas_gbq
import logging

# logger code block for pandas_gbq
logger = logging.getLogger('pandas_gbq')
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler())

# Read Docs for API Config description
# https://www.semrush.com/api-analytics/#backlinks_overview
ENDPOINT = 'https://api.semrush.com/analytics/v1/'
TYPE = 'backlinks_overview'
KEY = config.SEMRUSH_API
TARGET = 'apmex.com'
TARGET_TYPE = 'root_domain'
EXPORT_COLUMNS = ('ascore,total,domains_num,urls_num,ips_num,'
                  'ipclassc_num,follows_num,nofollows_num,sponsored_num,'
                  'ugc_num,texts_num,images_num,forms_num,frames_num')
GBQ_TABLE_IF_EXIST = 'append'


def main():
    date = datetime.now()

    api_url = (
        f'{ENDPOINT}'
        f'?type={TYPE}'
        f'&key={KEY}'
        f'&target={TARGET}'
        f'&target_type={TARGET_TYPE}'
        f'&export_columns={EXPORT_COLUMNS}'
    )
    print(api_url)

    # Load api call to pandas dataframe
    df = pd.read_csv(api_url, sep=';')

    # # Add date_called column to dataframe
    df['date_called'] = date
    # # Add primary reference domain
    df['primary_domain'] = TARGET

    # # Upload dataframe in memory to gbq
    project_id = 'sem-rush'
    table_id = 'Sem_rush.backlinks_overview'
    pandas_gbq.to_gbq(df, table_id, project_id=project_id, if_exists=GBQ_TABLE_IF_EXIST)


if __name__ == "__main__":
    main()
