# General Test For Python BigQuery Script

import config
import datetime
import pandas as pd
from google.cloud import bigquery

# bigq client object
client = bigquery.Client()

adwords = 'testing_domain_adwords_adwords.py'
dataset_id = 'Sem_rush'
table_id = 'test1'

dataset_ref = client.dataset(dataset_id)