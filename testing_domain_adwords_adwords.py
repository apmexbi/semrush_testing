# This report lists a domain’s competitors in paid search results.

import config
import pandas as pd

# Read Docs for API Config description
# https://www.semrush.com/api-analytics/#domain_adwords_adwords
TYPE = 'domain_adwords_adwords'
KEY = config.SEMRUSH_API
DOMAIN = 'apmex.com'
DATABASE = 'us'
DISPLAY_LIMIT = '5'
DISPLAY_OFFSET = None
EXPORT_ESCAPE = None
EXPORT_DECODE = '1'
DISPLAY_DATE = None
EXPORT_COLUMNS = 'Dn,Cr,Np,Ad,At,Ac,Or'
DISPLAY_SORT = None


def main():
    api_url = (
        f'https://api.semrush.com/'
        f'?type={TYPE}'
        f'&key={KEY}'
        f'&display_limit={DISPLAY_LIMIT}'
        f'&export_columns={EXPORT_COLUMNS}'
        f'&domain={DOMAIN}'
        f'&database={DATABASE}'
    )

    df = pd.read_csv('domain_adwords_adwords.csv', sep=';')
    # put dataframe manipulation here
    # compresses file to zip
    compression_opt = dict(method='zip', archive_name='test.csv')
    df.to_csv('result.zip', index=False, compression=compression_opt)


if __name__ == "__main__":
    main()
