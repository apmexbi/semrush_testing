# This report lists a domain’s competitors in paid search results.

import config
from datetime import datetime
import pandas as pd
import pandas_gbq
import logging

# logger code block for pandas_gbq
logger = logging.getLogger('pandas_gbq')
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler())

# Read Docs for API Config description
# https://www.semrush.com/api-analytics/#domain_adwords_adwords
ENDPOINT = 'https://api.semrush.com/'
TYPE = 'url_organic'
KEY = config.SEMRUSH_API
URL = 'https://sdbullion.com/'
DATABASE = 'us'
DISPLAY_LIMIT = '2000'
DISPLAY_OFFSET = None
EXPORT_ESCAPE = None
EXPORT_DECODE = None
DISPLAY_DATE = None
EXPORT_COLUMNS = 'Ph,Po,Pp,Nq,Cp,Co,Kd,Tr,Tg,Tc,Nr,Ts'
DISPLAY_SORT = None
DISPLAY_FILTER = None

GBQ_TABLE_IF_EXIST = 'append'
GBQ_PROJECT_ID = 'sem-rush'
GBQ_TABLE_ID = 'Sem_rush.url_organic_keywords'


def main():
    date = datetime.now()

    api_url = (
        f'{ENDPOINT}'
        f'?type={TYPE}'
        f'&key={KEY}'
        f'&url={URL}'
        f'&database={DATABASE}'
        f'&display_limit={DISPLAY_LIMIT}'
        f'&export_columns={EXPORT_COLUMNS}'
    )
    # print(api_url)

    # Load api call to pandas dataframe
    df = pd.read_csv(api_url, sep=';')

    df.rename(columns={'Keyword': 'keyword',
                       'Position': 'position',
                       'Previous Position': 'previous_position',
                       'Search Volume': 'search_volume',
                       'CPC': 'CPC',
                       'Competition': 'competition',
                       'Keyword Difficulty': 'keyword_difficulty',
                       'Traffic (%)': 'traffic_percent',
                       'Traffic': 'traffic',
                       'Traffic Cost (%)': 'traffic_cost_percent',
                       'Number of Results': 'number_of_results',
                       'Timestamp': 'timestamp'}, inplace=True)
    # Add date_called column to dataframe
    df['date_uploaded'] = date
    # Rename Column
    df['primary_domain'] = URL

    # Outputs dataframe to terminal
    pd.set_option('display.width', 1000)
    pd.set_option('display.max_columns', 1000)
    print(df)

    # Upload dataframe in memory to gbq
    pandas_gbq.to_gbq(df, GBQ_TABLE_ID, project_id=GBQ_PROJECT_ID, if_exists=GBQ_TABLE_IF_EXIST)


if __name__ == "__main__":
    main()

