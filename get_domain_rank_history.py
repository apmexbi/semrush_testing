# This report provides live and historical data on a domain’s
# keyword rankings in both organic and paid search in a chosen database.

import config
from datetime import datetime
import pandas as pd
import pandas_gbq
import logging

# logger code block for pandas_gbq
logger = logging.getLogger('pandas_gbq')
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler())

# Read Docs for API Config description
# https://www.semrush.com/api-analytics/#domain_rank_history
ENDPOINT = 'https://api.semrush.com/'
TYPE = 'domain_rank_history'
KEY = config.SEMRUSH_API
DOMAIN = 'sdbullion.com'
DATABASE = 'us'
DISPLAY_LIMIT = '100'
DISPLAY_OFFSET = None
EXPORT_ESCAPE = None
EXPORT_DECODE = None
DISPLAY_DAILY = None
EXPORT_COLUMNS = 'Rk,Or,Ot,Oc,Ad,At,Ac,Dt'

GBQ_TABLE_IF_EXIST = 'append'


def main():
    date = datetime.now()

    api_url = (
        f'{ENDPOINT}'
        f'?type={TYPE}'
        f'&key={KEY}'
        f'&domain={DOMAIN}'
        f'&database={DATABASE}'
        f'&display_limit={DISPLAY_LIMIT}'
        f'&export_columns={EXPORT_COLUMNS}'
    )
    # print(api_url)

    # Load api call to pandas dataframe
    df = pd.read_csv(api_url, sep=';')

    # Rename dataframe schema to match gbq standard
    df.rename(columns={'Rank': 'rank',
                       'Organic Keywords': 'organic_keywords',
                       'Organic Traffic': 'organic_traffic',
                       'Organic Cost': 'organic_cost',
                       'Adwords Keywords': 'adwords_keywords',
                       'Adwords Traffic': 'adwords_traffic',
                       'Adwords Cost': 'adwords_cost',
                       'Date': 'date'}, inplace=True)

    # Add date_called column to dataframe
    df['date_called'] = date
    # Add primary reference domain
    df['primary_domain'] = DOMAIN

    # Outputs dataframe to terminal
    # pd.set_option('display.width', 1000)
    # pd.set_option('display.max_columns', 1000)
    # print(df)

    # Upload dataframe in memory to gbq
    project_id = 'sem-rush'
    table_id = 'Sem_rush.domain_rank_history'
    pandas_gbq.to_gbq(df, table_id, project_id=project_id, if_exists=GBQ_TABLE_IF_EXIST)


if __name__ == "__main__":
    main()
