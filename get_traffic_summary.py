# This report allows you to get the main estimated traffic
# metrics for multiple domains on a monthly basis for desktop
# and mobile users in CSV format. To gain insights on your
# markets, prospects or partners, enter your target domain,
# use filters and get key traffic data: traffic rank, visits,
# unique visitors, pages per visit, avg. visit duration, and
# bounce rate.

import config
from datetime import datetime
import pandas as pd
import pandas_gbq
import logging

# logger code block for pandas_gbq
logger = logging.getLogger('pandas_gbq')
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler())

# Read Docs for API Config description
# https://www.semrush.com/api-analytics/#domain_adwords_adwords
ENDPOINT = 'https://api.semrush.com/analytics/ta/v2/'
TYPE = 'traffic_summary'
KEY = config.SEMRUSH_API
DOMAINS = 'apmex.com,jmbullion.com,providentmetals.com,sdbullion.com/'
DISPLAY_DATE = None
COUNTRY = 'US'
EXPORT_COLUMNS = 'domain,display_date,country,total_rank,desktop_rank,' \
                 'mobile_rank,desktop_share,mobile_share,total_visits,' \
                 'mobile_visits,desktop_visits,total_unique_visitors,' \
                 'mobile_unique_visitors,desktop_unique_visitors,' \
                 'total_pages_per_visit,mobile_pages_per_visit,' \
                 'desktop_pages_per_visit,total_avg_visit_duration,' \
                 'mobile_avg_visit_duration,desktop_avg_visit_duration,' \
                 'total_bounce_rate,mobile_bounce_rate,desktop_bounce_rate'

GBQ_TABLE_IF_EXIST = 'append'
GBQ_PROJECT_ID = 'sem-rush'
GBQ_TABLE_ID = 'Sem_rush.traffic_summary'

def main():
    date = datetime.now()

    api_url = (
        f'{ENDPOINT}'
        f'?type={TYPE}'
        f'&key={KEY}'
        f'&domains={DOMAINS}'
        f'&country={COUNTRY}'
        f'&export_columns={EXPORT_COLUMNS}'
    )
    print(api_url)

    # # Load api call to pandas dataframe
    # df = pd.read_csv(api_url, sep=';')
    #
    # df.rename(columns={'Keyword': 'keyword',
    #                    'Position': 'position',
    #                    'Previous Position': 'previous_position',
    #                    'Search Volume': 'search_volume',
    #                    'CPC': 'CPC',
    #                    'Competition': 'competition',
    #                    'Keyword Difficulty': 'keyword_difficulty',
    #                    'Traffic (%)': 'traffic_percent',
    #                    'Traffic': 'traffic',
    #                    'Traffic Cost (%)': 'traffic_cost_percent',
    #                    'Number of Results': 'number_of_results',
    #                    'Timestamp': 'timestamp'}, inplace=True)
    # # Add date_called column to dataframe
    # df['date_uploaded'] = date
    # # Rename Column
    # df['primary_domain'] = URL
    #
    # # Outputs dataframe to terminal
    # pd.set_option('display.width', 1000)
    # pd.set_option('display.max_columns', 1000)
    # print(df)
    #
    # # Upload dataframe in memory to gbq
    # pandas_gbq.to_gbq(df, GBQ_TABLE_ID, project_id=GBQ_PROJECT_ID, if_exists=GBQ_TABLE_IF_EXIST)


if __name__ == "__main__":
    main()

