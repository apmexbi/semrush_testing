# This report lists backlinks for a domain, root domain, or URL.

import config
from datetime import datetime
import pandas as pd
import pandas_gbq
import logging
from google.cloud import bigquery
from google.cloud import storage

# logger code block for pandas_gbq
logger = logging.getLogger('pandas_gbq')
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler())

# Read Docs for API Config description
# https://www.semrush.com/api-analytics/#backlinks
TYPE = 'backlinks'
KEY = config.SEMRUSH_API
TARGET = 'apmex.com'
TARGET_TYPE = 'root_domain'
EXPORT_COLUMNS = ('page_ascore,source_title,source_url,target_url,'
                  'anchor,response_code,source_size,external_num,'
                  'internal_num,first_seen,last_seen')
DISPLAY_SORT = None
DISPLAY_LIMIT = '50'
DISPLAY_OFFSET = None
DISPLAY_FILTER = None


def main():
    date = datetime.now()

    # file_name = date.strftime("%m_%d_%Y") + '_backlinks.csv'

    api_url = (
        f'https://api.semrush.com/analytics/v1/'
        f'?type={TYPE}'
        f'&key={KEY}'
        f'&target={TARGET}'
        f'&target_type={TARGET_TYPE}'
        f'&export_columns={EXPORT_COLUMNS}'
        f'&display_limit={DISPLAY_LIMIT}'
    )

    # Load api call to pandas dataframe
    df = pd.read_csv(api_url, sep=';')
    # Load dataframe to csv file
    # df.to_csv(file_name, index=False)

    # Add date_called column to dataframe
    df['date_called'] = date

    # Outputs dataframe to terminal
    # pd.set_option('display.width', 1000)
    # pd.set_option('display.max_columns', 1000)
    # print(df)

    # Upload dataframe in memory to gbq
    project_id = 'sem-rush'
    table_id = 'Sem_rush.backlinks'
    pandas_gbq.to_gbq(df, table_id, project_id=project_id, if_exists='replace')

    # BIGQUERY LOCAL CSV TO GBQ
    # dataset_id = 'sem_rush_test'
    # table_id = 'test_table1'
    # client = bigquery.Client()
    # # might need to update this line of code later
    # dataset_ref = client.dataset(dataset_id)
    # table_ref = dataset_ref.table(table_id)
    # job_config = bigquery.LoadJobConfig()
    # job_config.source_format = bigquery.SourceFormat.CSV
    # job_config.skip_leading_rows = 1
    # job_config.autodetect = True
    #
    # with open(file_name, "rb") as source_file:
    #     job = client.load_table_from_file(source_file, table_ref, job_config=job_config)
    # job.result()


if __name__ == "__main__":
    main()