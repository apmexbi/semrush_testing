# This report lists a domain’s competitors in organic search results.

import config
from datetime import datetime
import pandas as pd
import pandas_gbq
import logging

# logger code block for pandas_gbq
logger = logging.getLogger('pandas_gbq')
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler())

# Read Docs for API Config description
# https://www.semrush.com/api-analytics/#domain_organic_organic
ENDPOINT = 'https://api.semrush.com/'
TYPE = 'domain_organic_organic'
KEY = config.SEMRUSH_API
DOMAIN = 'apmex.com'
DATABASE = 'us'
DISPLAY_LIMIT = '1000'
DISPLAY_OFFSET = None
EXPORT_ESCAPE = None
EXPORT_DECODE = None
DISPLAY_DATE = None
EXPORT_COLUMNS = 'Dn,Cr,Np,Or,Ot,Oc'
DISPLAY_SORT = None
GBQ_TABLE_IF_EXIST = 'append'


def main():
    date = datetime.now()

    api_url = (
        f'{ENDPOINT}'
        f'?type={TYPE}'
        f'&key={KEY}'
        f'&domain={DOMAIN}'
        f'&database={DATABASE}'
        f'&display_limit={DISPLAY_LIMIT}'
        f'&export_columns={EXPORT_COLUMNS}'
    )

    # Load api call to pandas dataframe
    df = pd.read_csv(api_url, sep=';')

    # Add date_called column to dataframe
    df['date_uploaded'] = date
    # Rename Column
    df.rename(columns={'Domain': 'competitor_domain',
                       'Competitor Relevance': 'competitor_relevance',
                       'Common Keywords': 'common_keywords',
                       'Organic Keywords': 'organic_keywords',
                       'Organic Traffic': 'organic_traffic',
                       'Organic Cost': 'organic_cost'}, inplace=True)
    df['primary_domain'] = DOMAIN

    # Outputs dataframe to terminal
    pd.set_option('display.width', 1000)
    pd.set_option('display.max_columns', 1000)
    print(df)

    # Upload dataframe in memory to gbq
    project_id = 'sem-rush'
    table_id = 'Sem_rush.competitors_organic_search'
    pandas_gbq.to_gbq(df, table_id, project_id=project_id, if_exists=GBQ_TABLE_IF_EXIST)


if __name__ == "__main__":
    main()
