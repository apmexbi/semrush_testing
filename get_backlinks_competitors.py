# A list of domains with a similar backlink profile to the analyzed domain.

import config
from datetime import datetime
import pandas as pd
import pandas_gbq
import logging

# logger code block for pandas_gbq
logger = logging.getLogger('pandas_gbq')
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler())

# Read Docs for API Config description
# https://www.semrush.com/api-analytics/#backlinks
ENDPOINT = 'https://api.semrush.com/analytics/v1/'
TYPE = 'backlinks_competitors'
KEY = config.SEMRUSH_API
TARGET = 'apmex.com'
TARGET_TYPE = 'root_domain'
EXPORT_COLUMNS = ('ascore,neighbour,similarity,common_refdomains,'
                  'domains_num,backlinks_num')
DISPLAY_LIMIT = '1000'
DISPLAY_OFFSET = None
GBQ_TABLE_IF_EXIST = 'append'


def main():
    date = datetime.now()

    api_url = (
        f'{ENDPOINT}'
        f'?type={TYPE}'
        f'&key={KEY}'
        f'&target={TARGET}'
        f'&target_type={TARGET_TYPE}'
        f'&export_columns={EXPORT_COLUMNS}'
        f'&display_limit={DISPLAY_LIMIT}'
    )
    print(api_url)

    # Load api call to pandas dataframe
    df = pd.read_csv(api_url, sep=';')

    # Add date_called column to dataframe
    df['date_called'] = date
    # Add primary reference domain
    df['primary_domain'] = TARGET

    # Upload dataframe in memory to gbq
    project_id = 'sem-rush'
    table_id = 'Sem_rush.backlink_competitors'
    pandas_gbq.to_gbq(df, table_id, project_id=project_id, if_exists=GBQ_TABLE_IF_EXIST)


if __name__ == "__main__":
    main()
